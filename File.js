import React from 'react';
import PropTypes from 'prop-types';

import {Formik, Field, Form as FormikForm, ErrorMessage} from 'formik';

import {uniqueId} from 'libs/string';

//import Text from 'text';
import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import './File.scss';

class File extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'draganddrop';

		this.state = {files: null, error: ''};
	}

	onDragOver = ev => {
		ev.preventDefault();
		ev.stopPropagation();

		this.target.classList.add('active');
	};
	onDragLeave = ev => {
		ev.preventDefault();
		ev.stopPropagation();
		this.target.classList.remove('active');
	};

	onDrop = ev => {
		const {isMultiple} = this.props;

		const dt = ev.dataTransfer;
		const files = dt.files;

		const canDrop = isMultiple || files.length <= 1;

		if (canDrop) {
			this.input.files = files;

			const x = [...files];
			const dd = [];

			x.forEach((file, index) => {
				const reader = new FileReader();
				reader.onloadend = ev => {
					const data = ev.currentTarget.result;

					dd.push(<img key={`preview-${index}`} src={data} />);

					if (index === x.length - 1) {
						this.setState({files: dd, error: ''});
					}
				};
				reader.readAsDataURL(file);
			});
		} else {
			this.setState({error: 'You can only select a single file'});
		}
	};

	componentDidMount() {
		['dragenter', 'dragover'].forEach(event => {
			this.target.addEventListener(event, this.onDragOver, false);
		});

		['dragleave', 'drop'].forEach(event => {
			this.target.addEventListener(event, this.onDragLeave, false);
		});

		this.target.addEventListener('drop', this.onDrop, false);
	}

	innerRef = el => {
		this.input = el;
	};

	render() {
		const {label} = this.props;

		const {files, error} = this.state;
		const {isMultiple} = this.props;

		const id = this.props.id || uniqueId('field-');

		return (
			<div className="field">
				{label && (
					<label htmlFor={id}>
						<Text content={label} />
					</label>
				)}

				<div className={`${this.baseClass}`} ref={target => (this.target = target)}>
					<h4>drag and drop</h4>
					<Text content={error} />
					<div className={`${this.baseClass}__previews`}>{files}</div>
				</div>
				<Field {...this.props} id={id} type="file" component="input" innerRef={this.innerRef} />
				<ErrorMessage name={this.props.name} component="div" className="field__error" />
			</div>
		);
	}
}

File.defaultProps = {};

File.propTypes = {};

export default File;
