import React from 'react';
import PropTypes from 'prop-types';

// Date pickers
// https://www.smashingmagazine.com/2017/07/designing-perfect-date-time-picker/

// https://react-day-picker.js.org/examples/input-date-fns

// checkbox+radio
// https://codesandbox.io/s/pjqp3xxq7q

// https://codesandbox.io/s/github/turner-industries/formik-semantic-ui/tree/master/

// https://jaredpalmer.com/formik/docs/overview
// https://github.com/jaredpalmer/formik/blob/master/examples/MultistepWizard.js

import {Formik, Field, Form as FormikForm, ErrorMessage} from 'formik';

import Button from 'button';
import Loader from 'loader';

import Toast from 'toast';

// import * as Analytics from 'analytics';

import './Form.scss';

import axios from 'axios';

import {uniqueId} from 'libs/string';

//import Text from 'text';
//import Content from 'content';
import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import File from './file';

import Alert from 'alert';

import * as yup from 'yup';
import warning from 'tiny-warning';

import {isObject, isBoolean} from 'libs/is';

const FEEDBACK_TYPE = {
	ALL: 1,
	TOAST: 2,
	FORM: 3
};

/**
 * Form
 * @description [Description]
 * @example
  <div id="Form"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Form, {
        title : 'Example Form'
    }), document.getElementById("Form"));
  </script>
 */
class Form extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			feedbackMessage: '',
			isLoading: false,
			isSent: props.isSent
		};

		this.baseClass = 'form';
	}

	onBeforeUnload = ev => {
		// TODO: this
		warning('To hook up form tracking');
	};

	componentDidMount() {
		window.addEventListener('beforeunload', this.onBeforeUnload);
	}

	componentWillUnmount() {
		window.removeEventListener('beforeunload', this.onBeforeUnload);
	}

	componentWillReceiveProps(nextProps) {}

	onFailure(data = {}) {
		if (DEBUG) {
			console.log('onFailure', data);
		}

		const {onFailure, dictionary, feedbackType, errorMessage} = this.props;

		if (feedbackType === FEEDBACK_TYPE.ALL || feedbackType === FEEDBACK_TYPE.TOAST) {
			Toast.create(dictionary.sendingFailure, {
				id: 'form-status',
				timer: 8000,
				type: 'error',
				isDismissable: true
			});
		} else {
			// TODO: remove loading toast
		}



		this.setState({isLoading: false, isSent: false, feedbackMessage: data.message || errorMessage});

		if (onFailure) {
			onFailure(data);
		}
	}

	onSuccess(data = null) {
		if (DEBUG) {
			console.log('onSuccess', data);
		}

		const {dictionary, feedbackType, successMessage} = this.props;

		if (feedbackType === FEEDBACK_TYPE.ALL || feedbackType === FEEDBACK_TYPE.TOAST) {
			Toast.create(dictionary.sendingSuccess, {
				id: 'form-status',
				timer: 8000,
				type: 'success',
				isDismissable: true
			});
		} else {
			// TODO: remove loading toast
		}

		this.setState({isLoading: false, isSent: true, feedbackMessage: data.message || successMessage});

		const {onSuccess} = this.props;

		if (onSuccess) {
			onSuccess(data);
		}
	}

	onSubmit = (values, {setSubmitting, resetForm}) => {
		const {method, endpoint, dictionary} = this.props;

		if (DEBUG) {
			console.log(JSON.stringify(values, null, 2));
		}

		this.__loadingTimer = setTimeout(() => {
			Toast.create(
				<span>
					<Loader theme="dark" /> <Text content={dictionary.sendingStatus} />
				</span>,
				{id: 'form-status', isDismissable: false}
			);

			this.setState({isLoading: true});

			if (this.__loadingTimer) {
				clearTimeout(this.__loadingTimer);
				this.__loadingTimer = null;
			}
		}, 400);

		if (endpoint) {
			axios({
				method,
				url: endpoint,
				[method === 'get' ? 'params' : 'data']: values
			})
				.then(result => {
					const data = (result && result.data) || false;

					const isSuccess = isObject(data) && isBoolean(data.success) ? data.success : true;

					if (data && isSuccess) {
						this.onSuccess(data);

						// TODO: calling this results in the component being unmounted...
						// resetForm(this.initialValues);
					} else {
						this.onFailure(data);
					}
				})
				.catch(error => {
					if (DEBUG) {
						console.log(error);
					}

					this.onFailure();
				})
				.finally(() => {
					setSubmitting(false);

					if (this.__loadingTimer) {
						clearTimeout(this.__loadingTimer);
						this.__loadingTimer = null;
					}
				});
		} else {
			warning('No endoing defined');
		}
	};

	getFormValidationSchema() {
		const {schema} = this.props;

		return schema;
	}

	get formikprops() {
		const props = {
			initialValues: this.initialValues,
			onSubmit: this.onSubmit
		};

		const schema = this.getFormValidationSchema();

		if (schema) {
			props.validationSchema = schema;
		} else {
			props.validate = this.validate;
		}

		return props;
	}

	validate = values => {
		return {};
	};

	get initialValues() {
		return this.props.initialValues || {};
	}

	renderFields() {
		const {fields} = this.props;

		const tags = {
			text: Form.Text,
			checkbox: Form.Checkbox,
			color: Form.Color,
			date: Form.Date,
			'datetime-local': Form.DatetimeLocal,
			email: Form.Email,
			file: Form.File,
			hidden: Form.Hidden,
			month: Form.Month,
			number: Form.Number,
			password: Form.Password,
			radio: Form.Radio,
			range: Form.Range,
			search: Form.Search,
			tel: Form.Tel,
			text: Form.Text,
			time: Form.Time,
			url: Form.Url,
			week: Form.Week,
			select: Form.Select,
			textarea: Form.Textarea
		};

		if (fields) {
			return fields.map((item, id) => {
				const Tag = tags[item.type] || Form.Input;

				return <Tag key={`field-${id}`} {...item} />;
			});
		}

		return null;
	}

	renderErrors(errors) {
		if (Object.entries(errors).length === 0 && errors.constructor === Object) {
			return null;
		}

		const issues = Object.keys(errors).map(key => (
			<li key={key}>
				<Text content={errors[key]} />
			</li>
		));

		if (issues.length === 0) {
			return null;
		}

		return (
			<Alert type="error" isDismissable={false}>
				<ul>{issues}</ul>
			</Alert>
		);
	}

	render() {
		const {isLoading, isSent, feedbackMessage} = this.state;
		const {
			submitLabel,
			submitProps,
			resetLabel,
			method,
			url,
			encoding,
			hasErrorOverview,
			feedbackType,
			isHideOnSuccess
		} = this.props;

		const fields = this.renderFields();

		if (!fields) {
			return null;
		}

		const submit = {
			...{
				style: 'primary',
				label: submitLabel
			},
			...submitProps
		};

		const showForm = !isSent || (isSent && !isHideOnSuccess);

		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				{feedbackMessage && (feedbackType === FEEDBACK_TYPE.ALL || feedbackType === FEEDBACK_TYPE.FORM) && (
					<Alert type={isSent ? 'success' : 'error'}><Content content={feedbackMessage} /></Alert>
				)}
				{showForm && (
				<Formik {...this.formikprops}>
					{({values, isSubmitting, handleReset, dirty, errors, submitCount, ...rest}) => (
						<FormikForm method={method} action={url} encType={encoding} noValidate={true}>
							{hasErrorOverview && submitCount > 0 && this.renderErrors(errors)}

							<div className={`${this.baseClass}__main`}>{fields}</div>

							<div className="field field--buttons">
								{resetLabel && (
									<Button
										label={resetLabel}
										type="button"
										style="secondary"
										onClick={handleReset}
										isDisabled={!dirty || isSubmitting}
									/>
								)}

								<Button type="submit" isLoading={isLoading} isDisabled={isSubmitting} {...submit} />
							</div>
							{/*
							<ComponentInfo
								{...rest}
								values={values}
								isSubmitting
								dirty
								errors={errors}
								submitCount={submitCount}
							/>
							*/}
						</FormikForm>
					)}
				</Formik>
				)}
			</div>
		);
	}
}

Form.Field = ({label = null, ...props}) => {
	if (props.type === 'hidden') {
		return <Field {...props} />;
	}

	const id = props.id || uniqueId('field-');

	if (props.type === 'checkbox') {
		if (props.options) {
			const fields = props.options.map((item, i) => {
				const id = i === 0 ? id : uniqueId('field-');
				return (
					<label key={`lbl-${id}`} htmlFor={id}>
						<Field {...props} id={id} value={item} /> <Text content={item} />
					</label>
				);
			});

			return (
				<div className="field">
					<label htmlFor={id}>
						<Text content={label} />
					</label>
					{fields}
					<ErrorMessage name={props.name} component="div" className="field__error" />
				</div>
			);
		}

		return (
			<div className="field">
				<label htmlFor={id}>
					<Field {...props} id={id} /> <Text content={label} />
				</label>
				<ErrorMessage name={props.name} component="div" className="field__error" />
			</div>
		);
	}

	if (props.type === 'radio') {
		return (
			<div className="field">
				<label htmlFor={id}>
					<Field {...props} id={id} /> <Text content={label} />
				</label>
				<ErrorMessage name={props.name} component="div" className="field__error" />
			</div>
		);
	}

	return (
		<div className="field">
			{label && (
				<label htmlFor={id}>
					<Text content={label} />
				</label>
			)}
			<Field {...props} id={id} />
			<ErrorMessage name={props.name} component="div" className="field__error" />
		</div>
	);
};

Form.Input = ({...props}) => {
	return Form.Field({component: 'input', ...props});
};

Form.Checkbox = ({...props}) => {
	return Form.Field({type: 'checkbox', component: 'input', ...props});
};

Form.Color = ({...props}) => {
	return Form.Field({type: 'color', component: 'input', ...props});
};

Form.Date = ({...props}) => {
	return Form.Field({type: 'date', component: 'input', ...props});
};

Form.DatetimeLocal = ({...props}) => {
	return Form.Field({type: 'datetime-local', component: 'input', ...props});
};

Form.Email = ({...props}) => {
	return Form.Field({type: 'email', component: 'input', ...props});
};

Form.File = ({...props}) => {
	return <File {...props} />;
	// return Form.Field({type: 'file', component: 'input', ...props});
};

Form.Hidden = ({...props}) => {
	return Form.Field({type: 'hidden', component: 'input', ...props});
};

Form.Month = ({...props}) => {
	return Form.Field({type: 'month', component: 'input', ...props});
};

Form.Number = ({...props}) => {
	return Form.Field({type: 'number', component: 'input', ...props});
};

Form.Password = ({...props}) => {
	return Form.Field({type: 'password', component: 'input', ...props});
};

Form.Radio = ({...props}) => {
	return Form.Field({type: 'radio', component: 'input', ...props});
};

Form.Range = ({...props}) => {
	return Form.Field({type: 'range', component: 'input', ...props});
};

Form.Search = ({...props}) => {
	return Form.Field({type: 'search', component: 'input', ...props});
};

Form.Select = ({...props}) => {
	return Form.Field({component: 'select', ...props});
};

Form.Tel = ({...props}) => {
	return Form.Field({type: 'tel', component: 'input', ...props});
};

Form.Text = ({...props}) => {
	return Form.Field({type: 'text', component: 'input', ...props});
};

Form.Textarea = ({...props}) => {
	return Form.Field({component: 'textarea', ...props});
};
Form.Time = ({...props}) => {
	return Form.Field({type: 'time', component: 'input', ...props});
};

Form.Url = ({...props}) => {
	return Form.Field({type: 'url', component: 'input', ...props});
};

Form.Week = ({...props}) => {
	return Form.Field({type: 'week', component: 'input', ...props});
};

Form.yup = yup;

Form.FEEDBACK_TYPE = FEEDBACK_TYPE;

Form.defaultProps = {
	successMessage: null,
	errorMessage: null,
	feedbackType: FEEDBACK_TYPE.ALL,
	dictionary: {
		sendingStatus: 'Submitting',
		sendingSuccess: 'Submitted',
		sendingFailure: 'Oops! Something went wrong'
	},
	isSent: false,
	hasErrorOverview: true,
	onSuccess: null,
	onFailure: null,
	method: 'get',
	encoding: undefined,
	url: '',
	endpoint: null,
	resetLabel: null,
	submitLabel: 'Submit',
	submitProps: {},
	hasReset: false,
	initialValues: null,
	schema: null,
	isHideOnSuccess : false
};

Form.propTypes = {
	successMessage: PropTypes.string,
	errorMessage: PropTypes.string,
	feedbackType: PropTypes.any,
	dictionary: PropTypes.object,
	isSent: PropTypes.bool,
	hasErrorOverview: PropTypes.bool,
	onSuccess: PropTypes.func,
	onFailure: PropTypes.func,
	method: PropTypes.string.isRequired,
	encoding: PropTypes.string,
	url: PropTypes.string.isRequired,
	endpoint: PropTypes.string,
	resetLabel: PropTypes.string,
	submitLabel: PropTypes.string.isRequired,
	submitProps: PropTypes.object,
	hasReset: PropTypes.bool,
	initialValues: PropTypes.object,
	schema: PropTypes.object,isHideOnSuccess:PropTypes.bool
};

export default Form;
